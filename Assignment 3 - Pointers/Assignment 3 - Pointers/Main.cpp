
// Assignment 3 - Pointers
// Izaac Dewilde


#include <iostream>
#include <conio.h>

using namespace std;

void SwapIntegers(int *swap1, int *swap2)
{
	int SwapTemp;

	SwapTemp = *swap1;
	*swap1 = *swap2;
	*swap2 = SwapTemp;
}


// Do not modify the main function!
int main()
{
	int first = 0;
	int second = 0;

	cout << "Enter the first integer: ";
	cin >> first;

	cout << "Enter the second integer: ";
	cin >> second;

	cout << "\nYou entered:\n";
	cout << "first: " << first << "\n";
	cout << "second: " << second << "\n";

	SwapIntegers(&first, &second);

	cout << "\nAfter swapping:\n";
	cout << "first: " << first << "\n";
	cout << "second: " << second << "\n";

	cout << "\nPress any key to quit.";

	(void)_getch();
	return 0;
}
